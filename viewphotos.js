jQuery(document).ready(function (e) {
    jQuery("pre view").before("<div class='viewheader'><a class='view-text' data-clipboard-target='pre view' data-clipboard-action='view'>Click To See Full Size Images</a></div>"), e(".view-text").tooltip({ trigger: "click" });
});
var clipboard = new ClipboardJS(".view-text", {
    target: function (e) {
        return e.parentNode.nextElementSibling;
    },
});
function setTooltip(e, i) {
    $(e).tooltip("hide").attr("data-original-title", i).tooltip("show");
}
function hideTooltip(e) {
    setTimeout(function () {
        $(e).tooltip("hide");
    }, 1e3);
}
clipboard.on("success", function (e) {
    var i = $(e.trigger);
    setTooltip(i, "Copied"), hideTooltip(i);
}),
    clipboard.on("error", function (e) {
        var i = $(e.trigger);
        setTooltip("Failed"), hideTooltip(i);
    });